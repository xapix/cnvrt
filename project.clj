(defproject io.xapix/cnvrt "0.1.1-SNAPSHOT"
  :description "A Clojure library bundling some conversion functions."
  :url "https://gitlab.com/xapix/cnvrt"
  :license {:name "Eclipse Public License"
            :url  "http://www.eclipse.org/legal/epl-v10.html"}
  :deploy-repositories [["releases" :clojars]]
  :dependencies [[org.clojure/clojure "1.8.0"]
                 [org.codehaus.woodstox/woodstox-core-asl "4.4.1"]])
