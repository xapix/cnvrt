(ns io.xapix.cnvrt.conversion-test
  (:require [clojure.test :refer :all]
            [io.xapix.cnvrt.conversion :refer [ensure-its-sequential
                                               parse-number
                                               convert-value
                                               convert-hashmap-values]]))

(deftest ensure-its-sequential-test
  (is (= [1 2 :a]
         (ensure-its-sequential [1 2 :a])))
  (is (= [1 2 :a]
         (ensure-its-sequential '(1 2 :a))))
  (is (list? (ensure-its-sequential '(1 2 :a))))
  (is (= [{:foo 1 :bar 2}]
         (ensure-its-sequential {:foo 1 :bar 2})))
  (is (nil? (ensure-its-sequential nil)))
  (is (= [] (ensure-its-sequential [])))
  (is (thrown-with-msg? IllegalArgumentException #"Wrong input"
                        (ensure-its-sequential "Hello world"))))

(deftest parse-number-test
  (is (= 1
         (parse-number 1)
         (parse-number "1")))
  (is (= 1.0
         (parse-number 1.0)
         (parse-number "1.0")))
  (is (thrown? NumberFormatException
               (parse-number "test"))))

(deftest convert-value-test
  (is (= nil
         (convert-value nil :string)))
  (testing "to String"
    (is (= "1"
           (convert-value 1 :string)))
    (is (= "1.0"
           (convert-value 1.0 :string)))
    (is (= "true"
           (convert-value true :string))))
  (testing "to Integer"
    (is (= 1
           (convert-value 1 :integer)))
    (is (= 1
           (convert-value 1.0 :integer)))
    (is (= 1
           (convert-value "1" :integer))))
  (testing "to Float"
    (is (= 1.0
           (convert-value 1 :float)))
    (is (= 1.0
           (convert-value 1.0 :float)))
    (is (= 1.0
           (convert-value "1" :float)))
    (is (= 1.1
           (convert-value "1.1" :float))))
  (testing "to Boolean"
    (is (= true
           (convert-value 1 :boolean)))
    (is (= false
           (convert-value 0 :boolean)))
    (is (= true
           (convert-value "true" :boolean)))
    (is (= false
           (convert-value "false" :boolean)))))

(deftest convert-hashmap-values-test
  (is (= {:test-string        "3"
          :test-integer       3
          :test-float         9.0
          :test-boolean       true
          :test-bool-from-int false}
         (convert-hashmap-values {:test-string        3
                                  :test-integer       "3"
                                  :test-float         "9"
                                  :test-boolean       "true"
                                  :test-bool-from-int 0}
                                 {:test-string        :string
                                  :test-integer       :integer
                                  :test-float         :float
                                  :test-boolean       :boolean
                                  :test-bool-from-int :boolean}))))

