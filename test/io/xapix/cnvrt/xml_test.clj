(ns io.xapix.cnvrt.xml-test
  (:require [clojure.test :refer :all]
            [io.xapix.cnvrt.xml :as conversion-xml :refer [xml->map
                                                           map->xml
                                                           write-node!]]
            [clojure.xml :as xml])
  (:import (java.io ByteArrayInputStream)
           (javax.xml.stream XMLStreamWriter XMLOutputFactory)))

;; Helpers
(defn parse-xml [^String s]
  (-> (.getBytes s)
      (ByteArrayInputStream.)
      (xml/parse)))

(defn xml-stream-writer-wrapper [callback]
  (with-out-str
    (let [output-factory (XMLOutputFactory/newInstance)
          writer         (.createXMLStreamWriter output-factory *out*)]
      (.writeStartElement writer "root")
      (callback writer)
      (.writeEndElement writer)
      (.close writer))))

(deftest all-tags-distinct?-test
  (let [xml1 (parse-xml "<data><h1> hello </h1> <h1> ciao </h1> </data>")
        xml2 (parse-xml "<data><h1> hello </h1> <h2> ciao </h2> </data>")]
    (is (= false (@#'conversion-xml/all-tags-distinct? (:content xml1))))
    (is (= true (@#'conversion-xml/all-tags-distinct? (:content xml2))))))

;; Tests
(deftest xml->map-test
  (testing "Nil case"
    (is (= nil
           (xml->map nil))))
  (testing "Plain string"
    (is (= "hello"
           (xml->map "hello"))))
  (testing "Empty map"
    (is (= {}
           (xml->map {}))))
  (testing "Simple map with just one tag and content of String"
    (is (= {:hello "bravo"}
           (xml->map (parse-xml "<hello>bravo</hello>")))))
  (testing "One nested tag with String content"
    (is (= {:div {:h1 "hello"}}
           (xml->map (parse-xml "<div><h1>hello</h1></div>")))))
  (testing "Two nested element, same tag"
    (is (= {:persons [{:person "Salvatore"} {:person "Giuseppe"}]}
           (xml->map (parse-xml "<persons><person>Salvatore</person><person>Giuseppe</person></persons>")))))
  (testing "Two nested element, different tag"
    (is (= {:person {:address "Parlermo", :name "Salvatore"}}
           (xml->map (parse-xml "<person><name>Salvatore</name><address>Parlermo</address></person>")))))
  (testing "No content, but attributes"
    (is (= {:person {:_attr {:address "Erice"
                             :name    "Concetta"}}}
           (xml->map (parse-xml "<person name=\"Concetta\" address=\"Erice\"> </person> "))))))

(deftest write-attribute-type!-test
  (testing "integer"
    (is (= "<root type=\"integer\"/>"
           (xml-stream-writer-wrapper #(@#'conversion-xml/write-attribute-type! % 3)))))
  (testing "float"
    (is (= "<root type=\"float\"/>"
           (xml-stream-writer-wrapper #(@#'conversion-xml/write-attribute-type! % 9.99)))))
  (testing "boolean"
    (is (= "<root type=\"boolean\"/>"
           (xml-stream-writer-wrapper #(@#'conversion-xml/write-attribute-type! % true))))
    (is (= "<root type=\"boolean\"/>"
           (xml-stream-writer-wrapper #(@#'conversion-xml/write-attribute-type! % false)))))
  (testing "array"
    (is (= "<root type=\"array\"/>"
           (xml-stream-writer-wrapper #(@#'conversion-xml/write-attribute-type! % [])))))
  (testing "blank"
    (is (= "<root/>"
           (xml-stream-writer-wrapper #(@#'conversion-xml/write-attribute-type! % ""))))
    (is (= "<root/>"
           (xml-stream-writer-wrapper #(@#'conversion-xml/write-attribute-type! % nil))))))

(deftest write-node!-test
  (testing "Plain"
    (testing "Value present"
      (is (= "<root>foo</root>"
             (xml-stream-writer-wrapper #(write-node! % "foo" nil))))
      (is (= "<root>:foo</root>"
             (xml-stream-writer-wrapper #(write-node! % :foo nil))))
      (is (= "<root type=\"integer\">4</root>"
             (xml-stream-writer-wrapper #(write-node! % 4 nil)))))
    (testing "Value nil"
      (is (= "<root nil=\"true\"/>"
             (xml-stream-writer-wrapper #(write-node! % nil nil))))))
  (testing "Map"
    (testing "Convert map"
      (is (= "<root><foo>Hello</foo><bar>World</bar><boo nil=\"true\"/></root>"
             (xml-stream-writer-wrapper #(write-node! % {:foo "Hello" :bar "World" :boo nil} nil)))))
    (testing "Convert empty map"
      (is (= "<root/>"
             (xml-stream-writer-wrapper #(write-node! % {} nil))))))
  (testing "Sequential"
    (testing "Convert vector"
      (is (= "<root type=\"array\"><story>Hello</story><story>World</story></root>"
             (xml-stream-writer-wrapper #(write-node! % ["Hello" "World"] "stories")))))
    (testing "Convert empty vector"
      (is (= "<root type=\"array\"/>"
             (xml-stream-writer-wrapper #(write-node! % [] "stories")))))))

(deftest map->xml-test
  (testing "Plain Object"
    (is (= "<?xml version='1.0' encoding='UTF-8'?><root><foo>abc</foo><bar type=\"integer\">345</bar></root>"
           (map->xml {:foo "abc", :bar 345}))))
  (testing "Nested Object"
    (is (= "<?xml version='1.0' encoding='UTF-8'?><root><data type=\"array\"><datum><foo>abc</foo><bar type=\"integer\">345</bar></datum><datum><foo>abc</foo><bar type=\"integer\">345</bar></datum></data></root>"
           (map->xml {:data [{:foo "abc", :bar 345}, {:foo "abc", :bar 345}]}))))
  (testing "Array of maps"
    (is (= "<?xml version='1.0' encoding='UTF-8'?><root type=\"array\"><root><foo>abc</foo><bar type=\"integer\">345</bar></root><root><foo>abc</foo><bar type=\"integer\">345</bar></root></root>"
           (map->xml [{:foo "abc", :bar 345}, {:foo "abc", :bar 345}]))))
  (testing "Array of nested maps"
    (is (= "<?xml version='1.0' encoding='UTF-8'?><root type=\"array\"><root><foo><test type=\"array\"><test><test type=\"array\"><test/></test><integer type=\"integer\">123</integer><float type=\"float\">0.3</float><boolean type=\"boolean\">true</boolean><string>test</string></test></test><integer type=\"integer\">123</integer><float type=\"float\">0.3</float><boolean type=\"boolean\">true</boolean><string>test</string></foo><bar><integer type=\"integer\">123</integer><float type=\"float\">0.3</float><boolean type=\"boolean\">true</boolean><string>test</string></bar></root></root>"
           (map->xml [{:foo {:test [{:test [{}], :integer 123, :float 0.3, :boolean true, :string "test"}], :integer 123, :float 0.3, :boolean true, :string "test"}, :bar {:integer 123, :float 0.3, :boolean true, :string "test"}}]))))
  (testing "Array of ints"
    (is (= "<?xml version='1.0' encoding='UTF-8'?><root type=\"array\"><root type=\"integer\">1</root><root type=\"integer\">2</root><root type=\"integer\">3</root><root type=\"integer\">4</root><root type=\"integer\">5</root></root>"
           (map->xml [1, 2, 3, 4, 5]))))
  (testing "Array of Arrays"
    (is (= "<?xml version='1.0' encoding='UTF-8'?><root type=\"array\"><root>a</root><root type=\"array\"><root>a-1</root><root>a-2</root></root><root>b</root></root>"
           (map->xml ["a", ["a-1", "a-2"], "b"]))))
  (testing "Map containing nil values"
    (is (= "<?xml version='1.0' encoding='UTF-8'?><root type=\"array\"><root><foo>abc</foo><bar type=\"integer\">345</bar></root><root><foo>abc</foo><bar nil=\"true\"/></root></root>"
           (map->xml [{:foo "abc", :bar 345}, {:foo "abc", :bar nil}])))))
