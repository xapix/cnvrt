# CNVRT

[![Clojars Project](https://img.shields.io/clojars/v/io.xapix/cnvrt.svg)](https://clojars.org/io.xapix/cnvrt)

A Clojure library bundling some conversion functions.  
Currently supporting 
- conversions from/to XML
- singularizing words and
- converting a map based on a simple schema. 
 

## Usage

### XML
Require using `(require [io.xapix.cnvrt.xml :as xml-conv])`.

**Convert a map to XML**

Root element can be set as 2nd parameter, defaults to `root`.

```clojure
(xml-conv/map->xml {:data [{:foo "abc", :bar 345}, {:foo "abc", :bar 345}]})
;; or
(xml-conv/map->xml {:data [{:foo "abc", :bar 345}, {:foo "abc", :bar 345}]} "root")
```
```xml
<?xml version="1.0" encoding="UTF-8"?>
<root>
   <data type="array">
      <datum>
         <foo>abc</foo>
         <bar type="integer">345</bar>
      </datum>
      <datum>
         <foo>abc</foo>
         <bar type="integer">345</bar>
      </datum>
   </data>
</root>
```

**Converts XML to a map**  

Expects the result of `clojure.xml/parse` as input.

```clojure
(def test-xml (-> "<person><name>Salvatore</name><address>Parlermo</address></person>"
                  (.getBytes)
                  (java.io.ByteArrayInputStream.)
                  (clojure.xml/parse)))
(xml-conv/xml->map test-xml)
```
```clojure
{:person {:address "Parlermo", :name "Salvatore"}}
```

### Inflector
Require using `(require [io.xapix.cnvrt.inflector :as inflector])`.

Currently only `singularize` is supported, which is a port of the RoR 
[ActiveSupport::Inflector](http://api.rubyonrails.org/classes/ActiveSupport/Inflector.html#method-i-singularize)
method.   
It accepts a string word and returns the singularized word.

```clojure
(inflector/singularize "archives") ; => "archive" 
(inflector/singularize "indices") ; => "index" 
```

### Conversion
Require using `(require [io.xapix.cnvrt.conversion :as conversion])`.

**ensure-its-sequential**  
If input is sequential, it will be returned.  
If input is a map, it will be wrapped in an vector.  
If input is nil, then result is nil as well.  
All other cases will throw `IllegalArgumentException`.  
````clojure
(conversion/ensure-its-sequential [:foo :bar]) ; => [:foo :bar]
(conversion/ensure-its-sequential {:foo :bar}) ; => [{:foo :bar}]
````

**parse-number**  
Tries to parse the given input to Long. In case it fails, it tries to parse to Double. 
If that also fails, `NumberFormatException` is thrown.
````clojure
(conversion/parse-number "1") ; => 1
(conversion/parse-number "1.1") ; => 1.1
````

**convert-value**  
Converts a given value to the the given output type. Allowed types are: string, integer, float, boolean.  
`nil` input returns `nil`, no matter of the output type.
````clojure
(conversion/convert-value "true" :boolean) ; => true
(conversion/convert-value "1.1" :float) ; => 1.1
````

**convert-hashmap-values**  
Works the same way as `convert-value`, but accepts two maps: One containing the values, one containing the schema.
```clojure
(conversion/convert-hashmap-values {:test-string        3
                                    :test-integer       "3"
                                    :test-float         "9"
                                    :test-boolean       "true"
                                    :test-bool-from-int 0}
                                   {:test-string        :string
                                    :test-integer       :integer
                                    :test-float         :float
                                    :test-boolean       :boolean
                                    :test-bool-from-int :boolean})
; => {:test-string        "3"
;     :test-integer       3
;     :test-float         9.0
;     :test-boolean       true
;     :test-bool-from-int false}
```

## License

(c) Xapix Inc. 

Distributed under the Eclipse Public License, the same as Clojure.
