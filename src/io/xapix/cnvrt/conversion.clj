(ns io.xapix.cnvrt.conversion
  (:require [clojure.string :as string]))

(defn ensure-its-sequential
  "Make sure data is either sequential or a map inside a vector.

  Will return nil if it receives a nil value"
  [data]
  (cond
    (sequential? data) data
    (map? data) [data]
    (nil? data) nil
    :else (throw (IllegalArgumentException. "Wrong input: expected vector or map"))))

(defn parse-number
  "Parses a number to Long; on failure parses to Double.
  Throws NumberFormatException if value cannot be converted to Long or Double."
  [value]
  (if (number? value)
    value
    (let [value (str value)]
      (try
        (Long/parseLong value)
        (catch NumberFormatException _
          (Double/parseDouble value))))))


(defmulti convert-value
  "Type casts a single value to the requested type."
  (fn [value type] (if (nil? value) :default
                                    type)))
(defmethod convert-value :string [value _] (str value))
(defmethod convert-value :integer [value _] (int (parse-number value)))
(defmethod convert-value :float [value _] (double (parse-number value)))
(defmethod convert-value :boolean [value _] (let [value-str (str value)]
                                              (boolean (some #(.equalsIgnoreCase value-str %)
                                                             ["true" "t" "1" "yes" "y" "on"]))))
(defmethod convert-value :default [value _] (when-not (nil? value)
                                              (throw (IllegalArgumentException. "Wrong input: illegal type cast type"))))


(defn convert-hashmap-values
  "Converts the values of a hash to the defined type.
  Types are defined using a flat map, where the key is
  the data attribute and value is one of the allowed data types.
  Example: `{:foo :string :bar :integer}`"
  [datum attributes-types]
  (reduce-kv #(assoc %1 %2 (convert-value %3 (get attributes-types %2)))
             {} datum))



