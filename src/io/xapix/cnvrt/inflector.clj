(ns io.xapix.cnvrt.inflector
  (:require [clojure.string :as string]))

;; Inspired by ActiveResource::Inflector
(defn singularize [^String plural]
  "Returns the singular form of an input word. List is not considered complete."
  (let [lc (.toLowerCase plural)]
    (condp re-find lc
      ;; Uncountable
      #"(equipment|information|rice|money|species|series|fish|sheep|jeans|police)$" (string/replace lc #"(equipment|information|rice|money|species|series|fish|sheep|jeans|police)$" "$1")

      ;; Irregulars
      #"people$" (string/replace lc #"people$" "person")
      #"men$" (string/replace lc #"men$" "man")
      #"children$" (string/replace lc #"children$" "child")
      #"sexes$" (string/replace lc #"sexes$" "sex")
      #"moves$" (string/replace lc #"moves$" "move")
      #"zombies$" (string/replace lc #"zombies$" "zombie")

      ;; Regulars
      #"(database)s$" (string/replace lc #"(database)s$" "$1")
      #"(quiz)zes$" (string/replace lc #"(quiz)zes$" "$1")
      #"(matr)ices$" (string/replace lc #"(matr)ices$" "$1ix")
      #"(vert|ind)ices$" (string/replace lc #"(vert|ind)ices$" "$1ex")
      #"^(ox)en" (string/replace lc #"^(ox)en" "$1")
      #"(alias|status)(es)?$" (string/replace lc #"(alias|status)(es)?$" "$1")
      #"(octop|vir)(us|i)$" (string/replace lc #"(octop|vir)(us|i)$" "$1us")
      #"^(a)x[ie]s$" (string/replace lc #"^(a)x[ie]s$" "$1xis")
      #"(cris|test)(is|es)$" (string/replace lc #"(cris|test)(is|es)$" "$1is")
      #"(shoe)s$" (string/replace lc #"(shoe)s$" "$1")
      #"(o)es$" (string/replace lc #"(o)es$" "$1")
      #"(bus)(es)?$" (string/replace lc #"(bus)(es)?$" "$1")
      #"^(m|l)ice$" (string/replace lc #"^(m|l)ice$" "$1ouse")
      #"(x|ch|ss|sh)es$" (string/replace lc #"(x|ch|ss|sh)es$" "$1")
      #"(m)ovies$" (string/replace lc #"(m)ovies$" "$1ovie")
      #"(s)eries$" (string/replace lc #"(s)eries$" "$1eries")
      #"([^aeiouy]|qu)ies$" (string/replace lc #"([^aeiouy]|qu)ies$" "$1y")
      #"([lr])ves$" (string/replace lc #"([lr])ves$" "$1f")
      #"(tive)s$" (string/replace lc #"(tive)s$" "$1")
      #"(hive)s$" (string/replace lc #"(hive)s$" "$1")
      #"([^f])ves$" (string/replace lc #"([^f])ves$" "$1fe")
      #"(^analy)(sis|ses)$" (string/replace lc #"(^analy)(sis|ses)$" "$1sis")
      #"((a)naly|(b)a|(d)iagno|(p)arenthe|(p)rogno|(s)ynop|(t)he)(sis|ses)$" (string/replace lc #"((a)naly|(b)a|(d)iagno|(p)arenthe|(p)rogno|(s)ynop|(t)he)(sis|ses)$" "$1sis")
      #"([ti])a$" (string/replace lc #"([ti])a$" "$1um")
      #"(n)ews$" (string/replace lc #"(n)ews$" "$1ews")
      #"(ss)$" (string/replace lc #"(ss)$" "$1")
      #"s$" (string/replace lc #"s$" "")
      lc)))
