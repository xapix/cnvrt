(ns io.xapix.cnvrt.xml
  (:require [io.xapix.cnvrt.inflector :as inflector])
  (:import (javax.xml.stream XMLStreamWriter XMLOutputFactory)))

;;; XML to map functions
;; Based on https://github.com/rosario/json-parse
(defn- all-tags-distinct?
  "This function returns true if all children tag names are distinct, false otherwise."
  [children]
  (when children
    (let [distinct-tags (->> (map :tag children)
                             (distinct)                     ; Remove duplicates
                             (filter identity))]            ; Remove nil values
      (= (count distinct-tags) (count children)))))

(defn xml->map [element]
  (cond
    ;; nil and string don't require transformation
    (or (nil? element) (string? element))
    element

    ;; handling lists
    (sequential? element)
    (cond
      ;; Only 0 or 1 element
      (<= (count element) 1)
      (xml->map (first element))

      ;; We can render it as map if *ALL* child tag names are distinct
      (all-tags-distinct? element)
      (reduce into {} (map (partial xml->map) element))

      ;; Render as sequence
      :else
      (map xml->map element))

    ;; handling empty maps
    (and (map? element) (empty? element))
    {}

    ;; handling non-empty maps
    (map? element)
    (let [attrs       (:attrs element)
          content-map (let [converted-el (xml->map (:content element))]
                        (cond
                          (or (map? converted-el) (empty? attrs)) converted-el
                          (empty? converted-el) {}
                          :else {:_value converted-el}))]
      {(:tag element) (if (not-empty attrs)
                        (assoc content-map :_attr attrs)
                        content-map)})

    ;; everything else is nil
    :else
    nil))


;;;
;;; Map to XML
;;;

(defn- write-attribute-type!
  "Adds a 'type=x' attribute to the current element, if `data` is of type integer, float, boolean or array."
  [writer data]
  (when-let [type (cond
                    (integer? data) "integer"
                    (float? data) "float"
                    (instance? Boolean data) "boolean"
                    (sequential? data) "array")]
    (.writeAttribute writer "type" type)))

(defmulti write-node!
  "Writes an element to XML.
  Accepts a map, vector or a plain element and passes it to the respective function."
  (fn [writer data root-element-name]
    (cond
      (map? data) :map
      (sequential? data) :sequential
      :else :plain)))

(defmethod write-node! :plain [writer data _]
  (write-attribute-type! writer data)
  (if (nil? data)
    (.writeAttribute writer "nil" "true")
    (.writeCharacters writer (str data))))

(defmethod write-node! :map [writer data _]
  (doseq [[el-key el-value] data]
    (let [key-name (name el-key)]
      (doto writer
        (.writeStartElement key-name)
        (write-node! el-value key-name)
        (.writeEndElement)))))

(defmethod write-node! :sequential [writer data root-element-name]
  (let [singularized-key (inflector/singularize root-element-name)]
    (write-attribute-type! writer data)
    (doseq [el data]
      (doto writer
        (.writeStartElement singularized-key)
        (write-node! el singularized-key)
        (.writeEndElement)))))

(defn map->xml
  "Accepts a Vector or Map, and returns a XML string.
  Heavily inspired by https://apidock.com/rails/Hash/to_xml"
  ([data]
   (map->xml data "root"))
  ([data root-element-name]
   {:pre [(or (map? data) (sequential? data))]}
   (with-out-str
     (doto (.createXMLStreamWriter (XMLOutputFactory/newInstance) *out*)
       (.writeStartDocument "UTF-8" "1.0")
       (.writeStartElement root-element-name)
       (write-node! data root-element-name)
       (.writeEndElement)
       (.close)))))
