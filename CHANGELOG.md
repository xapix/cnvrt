# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [Unreleased]
### Added
### Changed
### Deprecated
### Removed
### Fixed
### Security

## [0.1.0] - 2017-12-22
### Added
- `conversion` NS providing functions to convert values and hashes according to a schema.
- `xml` NS providing functions to convert a hash to XML string and vice versa. 
- `inflector` NS providing a function to singularize a word.

[Unreleased]: https://gitlab.com/xapix/clj-conversion/compare/0.1.0...HEAD
